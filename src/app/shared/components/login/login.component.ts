import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormGroup} from '@angular/forms';
import { Validators ,FormBuilder} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  submitted = false;
  loginForm:FormGroup;
  showSpinner=false;


  constructor(private router:Router,private fb:FormBuilder) {
  this.loginForm=fb.group({
    'email':[null,Validators.required],
    'password':[null,Validators.required]

  });
}

  ngOnInit(): void {
    document.body.classList.add('pedro');
  }


  ngOnDestroy():void{
    document.body.classList.remove('pedro');
  }

  loadData(){
    this.showSpinner = true ;
    setTimeout(() => {
      this.router.navigate(['dashboard']);
    }, 4000 );
  }



  onSubmit() {
    this.submitted = true;
  }

}
