import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './layouts/default/default.component';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { PostsComponent } from './modules/posts/posts.component';
import { OfficesComponent } from './modules/offices/offices.component'
import { LoginComponent } from './shared/components/login/login.component';
import { GoalComponent } from './modules/goal/goal.component';

const routes: Routes = [

  {path:'',component:LoginComponent},

  { path:'dashboard', component:DefaultComponent,
  children:[
    { path:'', component:DashboardComponent
  }
  ,
  { path: 'posts', component:PostsComponent },

  {path:'offices', component:OfficesComponent},

  {path:'goal', component:GoalComponent},

]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
