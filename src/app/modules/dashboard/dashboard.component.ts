import { Component} from '@angular/core';
import SwiperCore, { Virtual } from "swiper/core";
SwiperCore.use([Virtual]);



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  constructor() { }

  ngOnInit(): void {}

  // Create array with 1000 slides
  slides = Array.from({ length: 600 }).map((el, index) => `Slide ${index + 1}`);

  onSwiper(swiper) {
    console.log(swiper);
  }
  onSlideChange() {
    console.log('slide change');
  }
}

